import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {

  dataSource = STUDENT_DATA;

  constructor() { }
}

export interface IStudent {
  id : number;
  name: string;
  matricule: number;
}

export const STUDENT_DATA: IStudent[] = [
  {id: 1, name: 'Hydrogen', matricule: 1.0079},
  {id: 2, name: 'Hydrogen', matricule: 1.0079},
  {id: 3, name: 'Hydrogen', matricule: 1.0079},
  {id: 4, name: 'Hydrogen', matricule: 1.0079},
  {id: 5, name: 'Hydrogen', matricule: 1.0079},
  {id: 6, name: 'Hydrogen', matricule: 1.0079},
  {id: 7, name: 'Hydrogen', matricule: 1.0079},
  {id: 8, name: 'Hydrogen', matricule: 1.0079},
  {id: 9, name: 'Hydrogen', matricule: 1.0079},
  {id: 10, name: 'Hydrogen', matricule: 1.0079},
];

