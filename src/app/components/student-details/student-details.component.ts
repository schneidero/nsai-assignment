import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IStudent, STUDENT_DATA } from 'src/app/services/student-service.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  studentId ;

  student : IStudent ;

  constructor(private route : ActivatedRoute) { }

  ngOnInit() {
    this.studentId  = this.route.snapshot.params['id'];
    console.log(this.studentId);

    this.student = STUDENT_DATA[this.studentId - 1];
    console.log(this.student);
  }

}
