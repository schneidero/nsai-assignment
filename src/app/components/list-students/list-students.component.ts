import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/Models/student.model';
import { StudentServiceService } from 'src/app/services/student-service.service';
import { STUDENT_DATA} from 'src/app/services/student-service.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {
  
  displayedColumns: string[] = ['No' , 'name', 'matricule'];
  dataSource = STUDENT_DATA; 
  
  constructor(private studentService : StudentServiceService) { }

  ngOnInit() {
    
  }

}

