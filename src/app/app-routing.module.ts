import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ListStudentsComponent } from './components/list-students/list-students.component';
import { StudentDetailsComponent } from './components/student-details/student-details.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  {path : '' ,redirectTo : 'dashboard', pathMatch : 'full'},
  {path : 'all-students' , component :ListStudentsComponent},
  {path : 'student/:id' , component : StudentDetailsComponent},
  {path : 'dashboard', component : DashboardComponent},
  {path : '**', component : NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
